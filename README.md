# Oracle Cloud Demo 

# Create oracle account
[Link to create oracle account you get 3500 hours free](https://cloud.oracle.com/tryit)

# Create first instance

[Link to oracle tutorial](https://www.oracle.com/webfolder/technetwork/tutorials/obe/cloud/ocis/creating_vm/bmc_vm_tutorial.html)

## To ssh to oracle instance do the following

*ssh opc@ip_address —i private_key*

# Look up oracle provider information for terraform
[Here is the link to terraform providers for Oracle Cloud](https://www.terraform.io/docs/providers/opc/)

[Terraform Registry For Oracle](https://registry.terraform.io/modules/oracle)

# Now add necessary information to build out multiple instance

# Oracle SDK and Ansible
[Deploying with Ansible Playbooks](https://docs.cloud.oracle.com/iaas/Content/API/SDKDocs/ansiblegetstarted.htm)


# Kubernetes Deconstructed
[These are the slide for the talk below](http://kube-decon.carson-anderson.com/Layers/1-Basic.sozi.html#frame2595)

[Youtube clip of this talk](https://youtu.be/90kZRyPcRZw)

# Next we will build a kubnernetes cluster
[Kubernetes Engine this is a manual deployment](https://cloud.oracle.com/containers/kubernetes-engine)

[Oracle How to guide](https://cloud.oracle.com/iaas/whitepapers/kubernetes_on_oci.pdf)

